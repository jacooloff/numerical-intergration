
'''
HERE I USE EVAL()!!! PLEASE BE CAREFUL NOT TO WRITE ANYTHING EXCEPT EXPRESSION WHEN INPUT WILL ASK!!!
PLEASE WRITE THE EXPRESSION ONLY THE PYTHON WAY. Here is an example: 1/(1+x**2).

These code was prepared for my course work(Methods of numerical integration).
I used round() each time a calculation was made, the reason I did it is because 
Python's engine doesn't calculate float numbers the way we think. It reaches the approximate answer
and shows it like if it was 0.2*0.2 and the answer was 0.04 it's gonna show 0.04000000000000001.
'''

import math

def arrayMaker(a,b,y,n):
    if a<b:
        b,a = a,b
    h = round((a - b)/ n, 3) #Calculates the length between steps
    x = b #Step value (gonna change as for cycle goes)

    arrX = [] #Stores X values(steps) 
    arrY = [] #stores Y values

    for i in range(n+1):
        arrX.append(x) #Appends to the array on the outside
        arrY.append(round(eval(y),6))
        x= round(x+h,3)
    return arrX,arrY,h

def rightSideRectangle(a,b,y,n):
    
    arrX,arrY,h = arrayMaker(a,b,y,n)
    result = (round(sum(arrY[0:-1])*h,6)+round(sum(arrY[1:])*h,6))/2 
    return round(result, 6), arrX, arrY

def trapezoid(a,b,y,n):
    
    arrX,arrY,h = arrayMaker(a,b,y,n)    
    result = h*(round((arrY[0]+arrY[-1])/2,6)+round(sum(arrY[1:-1]),6))
    
    return round(result, 6), arrX, arrY

def simpson(a,b,y,n):
    
    arrX,arrY,h = arrayMaker(a,b,y,n)    
    result = round(h/3,6)*round((arrY[0]+4*(round(sum(arrY[1:-1:2]),6))+2*(round(sum(arrY[2:-1:2]),6))+arrY[-1]),6)
    
    return round(result,6), arrX, arrY
     
def threeEighth(a,b,y,n):
    
    arrX,arrY,h = arrayMaker(a,b,y,n)
        
    sum1 = round(arrY[0] + arrY[-1],6)
    
    sum2 = 0.0
    for i,y in enumerate(arrY):
        if i%3 == 0 or arrY[-1]%3 == 0:
            continue
        sum2 = round(sum2+y,6)
        
    sum3 = round(sum(arrY[3:-1:3]),6)
    
    result = round(3*h/8,6)*round(sum1+3*sum2+sum3,6)
    
    return round(result,6), arrX, arrY


'''HERE ARE THE PARAMETERS'''
a = float(input('Enter the first point: '))
b = float(input('Enter the second point: '))
y = input('Enter the integration expression: ')
n = int(input('Enter the number of steps. The more it is, the more precise is the answer:'))
print(rightSideRectangle(a,b,y,n))
print(trapezoid(a,b,y,n))
print(simpson(a,b,y,n))  
print(threeEighth(a,b,y,n))
